<?php
namespace App\Helpers;

use GuzzleHttp\Client;

class ImagesHelper{
    use License;
    /**
     * send post request upload Image to a different server
     * @return string image name
     */
    private function sendRequestToApi($tmpUpload,$name,$path){
      try {
        $client = new Client;
        $response = $client->request('POST', config('app.url_file_write'), 
          [                
                'multipart' => [
                      [
                          'name'     => 'name',
                          'contents' => $name,
                      ],                    
                      [
                          'name'     => 'fileUpload',
                          'contents' => fopen($tmpUpload, 'r'),
                      ],
                      [
                          'name'     => 'pathImage',
                          'contents' => $path,
                      ]
                  ],
                  'headers' => [
                      'Authorization' => 'Bearer '.env('UPLOAD_IMAGE_API_KEY'),
                  ],
          ]); 
        $body = (string)$response->getBody();
        \Log::info($body);

      } catch (\Exception $e) {
        \Log::info($e);
        return "error";
      }
    }

    public function uploadImage($file, $folder_upload) { 
        $pathFile = config('app.url_file_write');
        $name = $file->getClientOriginalName();
        $name = str_replace(" ", "-", $name);
        $pathImage = '/images/';
        $filename = strtotime('now') . strtolower($name);
        //dd(config('app.url_file_write'));
        // if (!file_exists($pathFile . $pathImage)) {
        //     mkdir($pathFile . $pathImage, 0777, true);
        // }
        $file->move("tmp-upload", $filename);
        $tmpUpload = "tmp-upload/".$filename;

        self::sendRequestToApi($tmpUpload,$filename,$pathImage);
        unlink("tmp-upload/".$filename);

        // die();
        return $pathImage . $filename;
    }

}
?>
